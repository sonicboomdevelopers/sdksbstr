//
//  jniDecoder.h
//  sonicboomlibrary
//
//  Created by 1-18 Golf on 22/07/19.
//  Copyright © 2019 1-18 Golf. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@class jniUtil;
@protocol MyDecoder

-(void)onRecognized:(NSObject*)thiz userId:(int)userId intMaxMultiplier:(int)intMaxMultiplier messageNum:(int)messageNum bandId:(int)bandId messageType:(int)messageType partnerId:(int)partnerId isNegative:(int)isNegative partNumber:(int)partNumber totalParts:(int)totalParts  message:(Byte*)message validationToken:(Byte*)validationToken ;
-(void)onInitVRCallback:(NSObject*)thiz packageId:(int)packageId typeId:(int)typeId;

@end
@interface jniDecoder : NSObject


@property (strong, nonatomic) jniUtil *util;
@property (nonatomic, strong) id <MyDecoder> mydec;

-(instancetype)init;
-(void)InitVRCallback:(int)packageId typeId:(int)typeId;
-(void) callback:(NSObject*)thiz freqK:(int)freqK decodedMessage:(char*)decodedMessage useErrorCorrection:(int)useErrorCorrection indexFreq:(int)indexFreq;
-(size_t)WriteCallback:(NSObject*)thiz hasConnection:(bool)hasConnection companyId:(int)companyId companyType:(int)companyType packageId:(int)packageId validTypeId:(int)validTypeId;
-(void) initVR:(NSObject*)thiz context_object:(NSObject*)context_object company:(NSString *)companyId app:(NSString *)appId developmentMode:(int)isDevelopmentMode mNativeDecode:(long)mNativeDecode;
-(void) uninitVR:(NSObject*)thiz;
-(void) startVR:(NSObject*)thiz sampleRate:(int)sampleRate tokenCount:(int)tokenCount;
-(void) setValidBandVR:(NSObject*)thiz validBandBit:(int)validBandBit;
-(void) stopVR:(NSObject*)thiz;
//-(void) decodeSB:(NSObject*)thiz audioData:(Byte*)javaAudioData sizeInBytes:(int)sizeInBytes;
-(void) decodeSB:(NSObject*)thiz audioData:(short*)javaAudioData sizeInBytes:(int)sizeInBytes;
-(void) enableDecodeHarmonicSB:(NSObject*)thiz isEnabling:(Boolean)isEnabling timeToMatch:(float)timeToMatch freqToStart:(int*)freqToStart numStartFreq:(int)numStartFreq;
-(int) getMaxEncoderIndex:(NSObject*)thiz;
-(bool)inRadius:(NSObject*)thiz lat1:(float)lat1 lon1:(float)lon1 lat2:(float)lat2 lon2:(float)lon2 radius:(float)radius;
-(float)distanceAP:(NSObject*)thiz signalLevelInDb:(float)signalLevelInDb freqInMhz:(float)freqInMhz;
@end

NS_ASSUME_NONNULL_END
